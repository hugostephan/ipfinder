package ipf;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileNotFoundException;

public class SearchEngine 
{
	private Config config ;
	private Logger logger ;
	
	private String search_string ;
	private String conn_string ;
	private String file_path ;
	private String filename ;
	private File f;
	private ActiveDirectory ad ;
	private User user ;
	
	public SearchEngine(Logger log, Config conf, String s_str)
	{
		this.search_string = s_str;
		this.logger = log ;
		this.config = conf ;
		this.conn_string = this.config.getSRV_AD() + this.config.getBASE_AD() ;
		this.file_path = this.config.getSRV_FIC() + this.config.getPATH_FIC() ;
		this.filename = this.file_path + search_string + ".txt" ;
	}
	
	public boolean searchFile()
	{
		boolean ret = false ;
		
		// First, we look for a filename matching exactly the search string
		this.f = new File(this.filename);
		if(f.exists())
		{
			ret = true ;
		}
		
		return ret ;
	}
	
	public User getUser() throws FileNotFoundException
	{
		User user = null ;
		
		if(this.searchFile())
		{
			user = new User (logger, this.filename);
			Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
			StringSelection content = new StringSelection(user.getIp());
			clpbrd.setContents(content, null);
		}
		
		return user ;
	}
}
