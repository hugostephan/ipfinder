package ipf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class User 
{
	private Logger logger ;
	
	private String login ;
	private String os ;
	private String hostname ;
	private String ip ;
	private String mac ;
	private String dhcp_host ;
	private String dns_host ;
	private String gateway ;
	private String mask ;
	private String [] net_access ;
	private String [] printer ;
	
	
	public String getLogin() 			{return login;}
	public String getOs() 				{return os;}
	public String getHostname() 		{return hostname;}
	public String getIp() 				{return ip;}
	public String getMac() 				{return mac;}
	public String getDhcp_host() 		{return dhcp_host;}
	public String getDns_host() 		{return dns_host;}
	public String getGateway() 			{return gateway;}
	public String getMask() 			{return mask;}
	public String[] getNet_access() 	{return net_access;}
	public String[] getPrinter() 		{return printer;}


	public User(Logger log, String filename) throws FileNotFoundException
	{
		this.logger = log ;
		File tmp_file = new File(filename);
		final Scanner scanner = new Scanner(tmp_file);
		while (scanner.hasNextLine()) 
		{
		   final String lineFromFile = scanner.nextLine();
		   if(lineFromFile.contains("Utilisateur")) 
		   { 
			   this.login = lineFromFile.split(" \\[ ")[1];
		   }
		   if(lineFromFile.contains("Systeme d'exploitation")) 
		   { 
			   this.os = lineFromFile.split(" \\[ ")[1];
		   }
		   if(lineFromFile.contains("Nom machine")) 
		   { 
			   this.hostname = lineFromFile.split(" \\[ ")[1];
		   }
		   if(lineFromFile.contains("Adresse IP")) 
		   { 
			   this.ip= lineFromFile.split(" \\[ ")[1];
		   }
		   if(lineFromFile.contains("Adresse MAC")) 
		   { 
			   this.mac = lineFromFile.split(" \\[ ")[1];
		   }
		   if(lineFromFile.contains("Serveur DHCP")) 
		   { 
			   this.dhcp_host = lineFromFile.split(" \\[ ")[1];
		   }
		   if(lineFromFile.contains("Serveur DNS")) 
		   { 
			   this.dhcp_host = lineFromFile.split(" \\[ ")[1];
		   }
		   if(lineFromFile.contains("Passerelle par d�faut"))
		   {
			   this.gateway = lineFromFile.split(" \\[ ")[1];
		   }
		   if(lineFromFile.contains("Masque de sous-r�seau"))
		   {
			   this.mask = lineFromFile.split(" \\[ ")[1];
		   }   
		}
		
		if( (this.ip == null || this.ip.equals("") ) && (this.hostname != null && !this.hostname.equals("")) )
		{
			logger.writeLog("Couldn't find IP in user file, host is probably down, but hostname is present. Trying to get IP using ping...", LEVEL.WARNING);
			logger.showLog("Le fichier utilisateur ne contient pas d'adresse IP. Tentative de r�cup�ration de l'IP � partir du nom de la machine...", LEVEL.WARNING);
			
			try 
			{
				Process p = Runtime.getRuntime().exec("ping -n 1 " + this.hostname);
		        p.waitFor();
		        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		        String line=reader.readLine();
		        while(!line.contains("Envoi"))
		        {
		        	System.out.println(line);
		        	line = reader.readLine();
		        }
		        Pattern pattern = Pattern.compile("\\[(.*?)\\]");
		        Matcher match = pattern.matcher(line);
		        if(match.find())
		        {
		        	logger.writeLog("IP found using ping hostname : " + match.group(1), LEVEL.SUCCESS);
		        	this.ip = match.group(1);
		        }
			}	
		    catch(Exception e)
			{
		    	logger.writeLog("Unable to get IP even using ping hostname.", LEVEL.CRITICAL);
		    	logger.showLog("Impossible de r�cup�rer l'IP de l'utilisateur.", LEVEL.CRITICAL);
			}
		}
	}
}
