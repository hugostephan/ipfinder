package ipf;


/**
 * Main app class, checks for config file, validation key, and starts logger and GUI
 * @author Hugo STEPHAN
 */
public class IpFinder 
{
	/**
	 * Program entry point
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// First we try to fetch configuration from file
		Config conf = new Config();
		Logger logger ;
		
		
		// If it's successful, we use fetched parameters to configure logger
		if(conf.isKEY_OK() && conf.isCONFIG_FOUND())
		{
			logger = new Logger(conf);
		}
		// Else we start logger with default params
		else
		{
			logger = new Logger("ipfinder.log", "|yyyy/MM/dd-HH:mm:ss|", true, true);
		}
				
		
		// Bad validation key error
		if(!conf.isKEY_OK())
		{
			logger.writeLog("Unrecoverrable IPFinder startup error.", LEVEL.CRITICAL);
			logger.writeLog(conf.getCONFIG_ERROR(), LEVEL.DETAILS);
			logger.writeLog("Your validation key is marked as corrupted. IPFinder is now stopping.", LEVEL.FATAL);
			logger.showLog("IPFinder ne peut pas d�marrer. Clef de validation corrompue.", LEVEL.FATAL);
		}
		// Can't read config file error
		else if(!conf.isCONFIG_FOUND())
		{
			logger.writeLog("Unrecoverrable IPFinder startup error.", LEVEL.CRITICAL);
			logger.writeLog("IPFinder configuration couldn't be load from specified configuration file, loading process ended up with following errors : \n" + conf.getCONFIG_ERROR(), LEVEL.DETAILS);
			logger.writeLog("Your validation key couldn't be found. IPFinder is now stopping.", LEVEL.FATAL);
			logger.showLog("IPFinder ne peut pas d�marrer. Fichier de configuration introuvable.", LEVEL.FATAL);
		}
		// If everything went fine we start GUI
		else
		{
			logger.writeLog("IPFinder configuration successfully loaded from file : "+conf.getConfigText(), LEVEL.INFO);
			logger.writeLog("IPFinder started successfully !", LEVEL.INFO);
			
			// Lauching app GUI
			new IpfGUI(conf, logger);
		}
	}
}
