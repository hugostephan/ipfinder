package ipf;

public enum LEVEL 
{
	SUCCESS, INFO, DETAILS, WARNING, CRITICAL, FATAL ;
}
