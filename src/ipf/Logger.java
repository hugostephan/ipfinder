package ipf;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;


/**
 * Logger class to write log properly in file and according to the configuration.
 * @author Hugo
 */
public class Logger 
{
	// Internal logger variables
	private String filename ;
	private InetAddress IP ;
	private DateFormat date_format ;
	private Date date_time ;
	private boolean debug ;
	private boolean visual ;
	
	
	/**
	 * Logger constructor test if configured log file is accessible and store the log filename and the debug modee.
	 * @param conf
	 */
	public Logger(Config conf)
	{
		this(conf.getLOG_FILENAME(), conf.getDATE_FORMAT(), conf.isDEBUG(), conf.isVISUAL_LOG());
	}

	
	/**
	 * 
	 * @param log_filename
	 * @param date_format_p
	 * @param debug_mode
	 */
	public Logger(String log_filename, String date_format_p, boolean debug_mode, boolean visual_log)
	{
		// First we try to get localhost IP
		try 
		{
			IP = InetAddress.getLocalHost();
		} 
		catch (UnknownHostException e1) 
		{
			System.out.println("Unable to get localhost address.");
			e1.printStackTrace();
		}
		
		
		// Then we test if configured log file is writable
		File tmp_file = new File(log_filename) ;
		try 
		{
			FileWriter fw = new FileWriter (tmp_file, true) ;
			this.filename = log_filename ;
		} 
		// If it is not, we use another logfile in the current app dir
		catch (IOException e) 
		{
			System.out.println("Unable to access specified log file : " + log_filename + ". \n" +
			"This is probably a permission issue, check if the configured log dir exists and if IP Finder is allowed to write in it. \n"+
			"Anyway Logger is now booting using is home dir as log dir and filename : ipfinder.log");
			this.filename = "ipfinder.log" ;
		}
		
		
		// Then we save the date format
		this.date_format = new SimpleDateFormat(date_format_p);
		date_time = new Date();
		String date_str = this.date_format.format(date_time) ;
		
		// Finally, we save logger modes
		this.debug = debug_mode;
		this.visual = visual_log;
		
		
		// Write the starting message in file
		// Writing the log string in log file
		FileWriter fw;
		try 
		{
			fw = new FileWriter(this.filename, true);
			fw.write("***************************************************************************************************************\n");
			fw.write("**  IP Finder is starting...                                                                                 **\n");
			fw.write("**  Date Time : "+date_str+         "                                                                        **\n");
			fw.write("***************************************************************************************************************\n");
			fw.close();
		} 
		catch (IOException e) 
		{
			System.out.println("Unable to access specified log file : " + this.filename + ". \n" +
			"This is probably a permission issue, check if the configured log dir exists and if IP Finder is allowed to write in it. \n"+
			"Anyway Logger is now booting using is home dir as log dir and filename : ipfinder.log");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Write a log message according to the configuration : if logger is in debug mode all logs are printed both on system.out and in log file, if logger is not in debug mode INFO LEVEL logs are ignored.
	 * @param msg the message to log
	 * @param lvl the error level linked to the message (see enum LEVEL : INFO, WARNING, CRITICAL, FATAL)
	 * @return true if the msg has been logged properly, false if problems occured while logging the message.
	 */
	public boolean writeLog(String msg, LEVEL lvl)
	{
		boolean ret = true ;
		
		// If not in debug mode, the logger doesn't print INFO LEVEL messages
		if( (!this.debug) && ((lvl == LEVEL.INFO) || (lvl == LEVEL.DETAILS)) )
		{
			return false;
		}
		
		
		// Preparing log string by concatenating a date string, the client IP and the message
		String log_string = "" ;
		Date date = new Date();
		String date_str = this.date_format.format(date) ;
		log_string += date_str + " - " + this.IP + " - " + lvl.name() + "  :  " + msg + " \n" ;
		
		
		// Writing the log string in log file
		FileWriter fw;
		try 
		{
			fw = new FileWriter(this.filename, true);
			fw.write(log_string);
			fw.close();
		} 
		catch (IOException e) 
		{
			System.out.println("Unable to access specified log file : " + this.filename + ". \n" +
			"This is probably a permission issue, check if the configured log dir exists and if IP Finder is allowed to write in it. \n"+
			"Anyway Logger is now booting using is home dir as log dir and filename : ipfinder.log");
			e.printStackTrace();
		}
		
		
		// If in debug mode, the logger duplicate the log message in system's standard output
		if (this.debug)
		{
			System.out.println(log_string);
		}
		
		
		// In visual mode, log are showed via dialog window
		if (this.visual)
		{
			this.showLog(msg, lvl);
		}
		
		return ret ;
	}
	
	
	/**
	 * Show a log message in a popup window
	 * @param msg the message to show
	 * @param lvl the error level of the message
	 */
	public void showLog(String msg, LEVEL lvl)
	{
		int visual_lvl = 0;
		String title = "";
		switch(lvl)
		{
			case SUCCESS:
				visual_lvl = JOptionPane.INFORMATION_MESSAGE;
				title = "Succ�s";
				break;			
			case INFO:
				visual_lvl = JOptionPane.INFORMATION_MESSAGE;
				title = "Information";
				break;
			case WARNING:
				visual_lvl = JOptionPane.WARNING_MESSAGE;
				title = "Avertissement";
				break;
			case DETAILS:
				visual_lvl = JOptionPane.INFORMATION_MESSAGE;
				title = "D�tails";
				break;
			case CRITICAL:
				visual_lvl = JOptionPane.ERROR_MESSAGE;
				title = "Erreur critique";
				break;
			case FATAL:
				visual_lvl = JOptionPane.ERROR_MESSAGE;
				title = "Erreur fatale";
				break;
		}
		JOptionPane.showMessageDialog(null, msg, title, visual_lvl);
	}
}
