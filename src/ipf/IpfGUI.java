package ipf;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;


/**
 * GUI Class handling graphical components configuration and events
 * @author Hugo STEPHAN
 */
public class IpfGUI extends JFrame implements ActionListener 
{
	private JButton submit ;
	private JTextField input ;
	private JEditorPane output ;
	private SearchEngine finder;
	private Config config ;
	private Logger logger ;
	private User user ;
	
	/**
	 * GUI Constructor, initialize components
	 * @param conf
	 */
	public IpfGUI(Config conf, Logger log)
	{
		super();

		// Global app config and logger
		this.config = conf ;
		this.logger = log ;
		
		
		// Label config
		JLabel titleLabel = new JLabel("Entrez un login utilisateur  :   ");
		titleLabel.setHorizontalAlignment(JLabel.CENTER);
		
		
		// Button config
		submit = new JButton(" OK ");
		submit.addActionListener(this);
		
		
		// Textfield config
		input = new JTextField();
		
		
		// Border config for text area
		Border raisedbevel = BorderFactory.createRaisedBevelBorder();
		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		Border border = BorderFactory.createCompoundBorder(raisedbevel, loweredbevel);
		
		
		// Text area config
		output = new JEditorPane("text/html", "");
		output.setBorder(border);
		output.setEditable(false);
		
		
		// Top pane config
		JPanel pane = new JPanel () ;
		pane.setLayout(new BoxLayout(pane, BoxLayout.X_AXIS));
		pane.add(titleLabel);
		pane.add(input);
		pane.add(Box.createRigidArea(new Dimension(5,0)));
		pane.add(submit);
		pane.add(Box.createRigidArea(new Dimension(5,0)));
		
		
		// Layout config
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(pane, BorderLayout.NORTH);
		this.getContentPane().add(output, BorderLayout.CENTER);
		
		ImageIcon ico = new ImageIcon(this.config.getPATH_ICON());
		this.setIconImage(ico.getImage());
		
		// Window config
		this.setTitle("IP Finder - Maire de Saint-Maur-des-Foss�s");
		this.setSize(450,120);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	
	/**
	 * Event handler function
	 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == this.submit)
		{
			String search_string = input.getText();

			if(search_string.equals("") || search_string == null)
			{
				logger.writeLog("No search string specified !", LEVEL.WARNING);
				logger.showLog("Veuillez saisir un login utilisateur � rechercher", LEVEL.WARNING);
				return ;
			}
			
			finder = new SearchEngine(this.logger, this.config, search_string);
			
			if(finder.searchFile())
			{
				try 
				{
					this.user = finder.getUser();
					
					logger.writeLog("IP address found for " +search_string+"  :  "+user.getIp(), LEVEL.SUCCESS);
					logger.showLog("Adresse IP trouv�e et copi�e dans le presse papier !\n"+search_string+"  :  "+user.getIp() , LEVEL.SUCCESS);
					
					String out_str = "<b>"; 
					out_str += "***********************************************************************************\n" ;
					out_str += "***  " +search_string+"  :  "+user.getIp() + "\n" ;
					out_str += "***********************************************************************************\n" ;
					out_str += "</b>";
					
					output.setText(out_str);
					Document document = output.getDocument();
					for (int index = 0; index + user.getIp().length() < document.getLength(); index++)
					{
						String match = document.getText(index, user.getIp().length());
	                    if (user.getIp().equals(match)) 
	                    {
	                    	javax.swing.text.DefaultHighlighter.DefaultHighlightPainter highlightPainter = new javax.swing.text.DefaultHighlighter.DefaultHighlightPainter(Color.GREEN);
	                        output.getHighlighter().addHighlight(index, index + user.getIp().length() - 1,highlightPainter);
	                    }
	                } 
				} 
				catch (FileNotFoundException e1) 
				{
					logger.showLog("Erreur : Impossible de lire les donn�es pour l'utilisateur " + search_string + ".", LEVEL.CRITICAL);
					logger.writeLog("FileNotFoundException occured when trying to read file.", LEVEL.CRITICAL);
				} 
				catch (BadLocationException e1) 
				{
					logger.showLog("Erreur : Impossible de s�lectionner l'adresse IP dans le champ de texte.", LEVEL.WARNING);
					logger.writeLog("BadLocationException occured when trying to select IP in output field.", LEVEL.WARNING);
				}
			}
			else
			{
				logger.showLog("Erreur : Aucune donn�e trouv�e pour l'utilisateur " + search_string + ". V�rifiez que vous avez correctement renseign� le login. \nSi oui, l'utilisateur n'utilise peut �tre pas (plus) cette session, vous pouvez consulter l'active directory pour plus d'informations.", LEVEL.CRITICAL);
				logger.writeLog("No data found for " + search_string + ", file doesn't exist.", LEVEL.CRITICAL);
			}
		}
	}
}
