package ipf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.naming.ConfigurationException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * Config class to get IPFinder configuration from JSON file and use it.
 * @author Hugo
 */
public class Config 
{
	// App config variables
	private String BASE_AD ;
	private String SRV_AD ;
	private String SRV_FIC ;
	private String PATH_FIC ;
	private String PATH_HOME ;
	private String LOG_FILENAME ;
	private String DATE_FORMAT ;
	private String PATH_ICON ;
	private boolean FULL_INFO ;
	private boolean DEBUG ;
	private boolean VISUAL_LOG ;
	
	
	// Config variables' accessors
	public String getSRV_AD() 			{return SRV_AD;}
	public String getBASE_AD() 			{return BASE_AD;}
	public String getSRV_FIC() 			{return SRV_FIC;}
	public String getPATH_FIC() 		{return PATH_FIC;}
	public String getPATH_HOME() 		{return PATH_HOME;}
	public String getLOG_FILENAME()		{return LOG_FILENAME;}
	public String getDATE_FORMAT()		{return DATE_FORMAT;}
	public String getPATH_ICON()		{return PATH_ICON;}
	public String getCONFIG_ERROR()		{return config_error_message;}
	public boolean isFULL_INFO() 		{return FULL_INFO;}
	public boolean isDEBUG()			{return DEBUG;}
	public boolean isVISUAL_LOG()		{return VISUAL_LOG;}
	public boolean isCONFIG_FOUND()		{return config_found;}
	public boolean isKEY_OK()			{return !bad_key;}

	
	// Internal config variables for internal class use 
	private String config_filename = "conf/ipfinder_conf.json";
	private File config_file ;
	private String config_error_message ;
	private boolean config_found ;
	private boolean bad_key ;
	
	
	/**
	 * Config constructor tries to fetch IPFinder configuration from json file. If it fails, the constructor will use default internal parameters.
	 */
	public Config() 
	{
		this.bad_key = false ;
		
		File tmp_file ;
		tmp_file = new File(this.config_filename);
		
		if(tmp_file.exists()) 
		{ 
		    this.config_file = new File(this.config_filename);
		    
		    JSONParser parser = new JSONParser();
		    try {
				Object obj = parser.parse(new FileReader(this.config_file));
				JSONObject config_json = (JSONObject) obj ;
				if(config_json.get("filename").equals("ipfinder_configuration_file"))
				{
					if(config_json.get("validation_key").equals("yL1AGE@oPE_qP6pEriGmA9jSpfJahJSpGrEJzg%0vbxOWT2[)44V}bJk)djJlYA?4?Afw)&oP<8F%yvJouHRi[<$a=ixTDyIe(*{Wk+t%JJPH3t"))
					{
						JSONObject parameters_json = (JSONObject) config_json.get("parameters");
						
						this.SRV_AD = (String) parameters_json.get("active_directory_host");
						this.BASE_AD = (String) parameters_json.get("active_directory_base");
						this.SRV_FIC = (String) parameters_json.get("file_storage_host");
						this.PATH_FIC = (String) parameters_json.get("file_storage_path");
						this.PATH_HOME = (String) parameters_json.get("ipfinder_home_directory");
						this.LOG_FILENAME = (String) parameters_json.get("ipfinder_log_filename");
						this.DATE_FORMAT = (String) parameters_json.get("ipfinder_date_format");
						this.PATH_ICON = (String) parameters_json.get("ipfinder_icon_path");
						this.FULL_INFO = (boolean) parameters_json.get("ipfinder_verbose_mode");
						this.DEBUG = (boolean) parameters_json.get("ipfinder_debug_mode");
						this.VISUAL_LOG = (boolean) parameters_json.get("ipfinder_visual_log");
						
						this.config_found = true ;
					}
					else
					{
						this.config_error_message = "Your configuration file's validation key :  '"+config_json.get("validation_key")+"' is incorrect.\n"+
													"IPFinder won't start and can't use your configuration file because it is now considered as corrupted." ;
						this.config_found = true ;
						this.bad_key = true ;
					}
				}
				else
				{
					throw new ConfigurationException () ;
				}
			} 
		    catch (FileNotFoundException e) 
		    {
				this.config_error_message = "IP Finder configuration file not found at location : " + this.config_filename +" and cannot be created. This might be a permission issue check the access rights and the existence of the log dir and log file. \n";
				this.config_found = false ;
			} 
		    catch (IOException e) 
		    {
		    	this.config_error_message = "IP Finder configuration file is not reachable or readable at location : " + this.config_filename + ". This might be a permission issue check the access rights and the existence of the log dir and log file.\n";
		    	this.config_found = false ;
			} 
		    catch (ParseException e) 
		    {
		    	this.config_error_message = "IP Finder configuration file contains JSON Syntax error at location : " + this.config_filename + ", you can check your JSON syntax online. \n";
				this.config_found = false ;
			} 
		    catch (ConfigurationException | NullPointerException e) 
		    {
		    	this.config_error_message = "IP finder configuration filename is corrupted and not valid so defaults parameters will be used. \n";
				this.config_found = false ;
			}
		}
	}
	
	
	/**
	 * Return a string containing a human readable and understandable text describing the actual configuration stored in the Config object
	 * @return a readable configuration string ready to be printed
	 */
	public String getConfigText()
	{
		String text = "\n";
		
		text += "* - active_directory_host   : " + this.SRV_AD + "\n" ;
		text += "* - active_directory_base   : " + this.BASE_AD + "\n" ;
		text += "* - file_storage_host       : " + this.SRV_FIC + "\n" ;
		text += "* - file_storage_path       : " + this.PATH_FIC + "\n" ;
		text += "* - ipfinder_home_directory : " + this.PATH_HOME + "\n" ;
		text += "* - ipfinder_log_filename   : " + this.LOG_FILENAME + "\n" ;
		text += "* - ipfinder_date_format    : " + this.DATE_FORMAT + "\n" ;
		text += "* - ipfinder_icon_path      : " + this.PATH_ICON + "\n" ;
		text += "* - ipfinder_verbose_mode   : " + this.FULL_INFO + "\n" ;
		text += "* - ipfinder_debug_mode     : " + this.DEBUG + "\n" ;
		text += "* - ipfinder_visual_log     : " + this.VISUAL_LOG ;
		
		return text ;
	}
}